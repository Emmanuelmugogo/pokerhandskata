/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mugoinc.pokerhandskata;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Emmanuelmugogo
 */
public class PokerHandsTest {

    String inputFile;

    @Before
    public void setUp() throws Exception {
        inputFile = "test.txt";
    }

    @Test
    public void createCard() throws Exception {
        String input = "KD";
        Card card = new Card(input);
        assertEquals(13, card.getValue());
        assertEquals('D', card.getSuit());
    }

    @Test
    public void createHand() throws Exception {
        String[] input = {"2D", "3D", "4D", "5D", "6D"};
        Hand hand = new Hand(input);
        assertEquals(2, hand.getValue(0));
        assertEquals('D', hand.getSuit(0));
        assertEquals(3, hand.getValue(1));
        assertEquals('D', hand.getSuit(1));
        assertEquals(4, hand.getValue(2));
        assertEquals('D', hand.getSuit(2));
        assertEquals(5, hand.getValue(3));
        assertEquals('D', hand.getSuit(3));
        assertEquals(6, hand.getValue(4));
        assertEquals('D', hand.getSuit(4));
    }

    @Test
    public void isPair() throws Exception {
        String content = "Black: 2H 5D 6S KC KD White: 2C 3H 4S 8C AH";
        writeToFile(content);

        PokerHands poker = new PokerHands(inputFile);
        poker.judgeHands();
        assertEquals(1, poker.player1.getCombination());
        assertEquals(13, poker.player1.getHighCard());
    }

    @Test
    public void isTwoPair() throws Exception {
        String content = "Black: 5H 5D 6S KC KD White: 2C 3H 4S 8C AH";
        writeToFile(content);

        PokerHands poker = new PokerHands(inputFile);
        poker.judgeHands();
        assertEquals(2, poker.player1.getCombination());
        assertEquals(13, poker.player1.getHighCard());
    }
    
    @Test
    public void isThreeOfAKind() throws Exception {
        String content = "Black: 2H 5D KS KC KD White: 2C 3H 4S 8C AH";
        writeToFile(content);

        PokerHands poker = new PokerHands(inputFile);
        poker.judgeHands();
        assertEquals(3, poker.player1.getCombination());
        assertEquals(13, poker.player1.getHighCard());
    } 
    
    @Test
    public void isStraight() throws Exception {
        String content = "Black: 2H 3D 4S 5C AD White: 2C 3H 4S 8C AH";
        writeToFile(content);

        PokerHands poker = new PokerHands(inputFile);
        poker.judgeHands();
        assertEquals(4, poker.player1.getCombination());
        assertEquals(5, poker.player1.getHighCard());
    } 
    
    @Test
    public void isFlush() throws Exception {
        String content = "Black: 2D 3D 4D 6D AD White: 2C 3H 4S 8C AH";
        writeToFile(content);

        PokerHands poker = new PokerHands(inputFile);
        poker.judgeHands();
        assertEquals(5, poker.player1.getCombination());
        assertEquals(14, poker.player1.getHighCard());
    } 
    
    @Test
    public void isFullHouse() throws Exception {
        String content = "Black: 3S 3D AC AS AD White: 2C 3H 4S 8C AH";
        writeToFile(content);

        PokerHands poker = new PokerHands(inputFile);
        poker.judgeHands();
        assertEquals(6, poker.player1.getCombination());
        assertEquals(14, poker.player1.getHighCard());
    } 
    
    @Test
    public void isFourOFAKind() throws Exception {
        String content = "Black: 3S AH AC AS AD White: 2C 3H 4S 8C KH";
        writeToFile(content);

        PokerHands poker = new PokerHands(inputFile);
        poker.judgeHands();
        assertEquals(7, poker.player1.getCombination());
        assertEquals(14, poker.player1.getHighCard());
    } 
    
    @Test
    public void isStraightFlush() throws Exception {
        String content = "Black: 2D 3D 4D 5D 6D White: 2C 3H 4S 8C KH";
        writeToFile(content);

        PokerHands poker = new PokerHands(inputFile);
        poker.judgeHands();
        assertEquals(8, poker.player1.getCombination());
        assertEquals(6, poker.player1.getHighCard());
    } 
    
    @Test
    public void getWinner() throws Exception {
        String content = "Black: 2D 3D 4D 5D 6D White: 2C 3H 4S 8C KH";
        writeToFile(content);

        PokerHands poker = new PokerHands(inputFile);
        poker.judgeHands();
        assertEquals("Black", poker.getWinner());
    } 
    
    @Test
    public void getTie() throws Exception {
        String content = "Black: 2D 3D 4D 5D 6D White: 2H 3H 4H 5H 6H";
        writeToFile(content);

        PokerHands poker = new PokerHands(inputFile);
        poker.judgeHands();
        assertEquals("Tie", poker.getWinner());
    } 

    private void writeToFile(String content) {
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(inputFile));
            writer.write(content);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}