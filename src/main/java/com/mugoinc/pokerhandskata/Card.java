/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mugoinc.pokerhandskata;

/**
 *
 * @author Emmanuelmugogo
 */
class Card {
    private String name;
    private int value;
    private char suit;

    //Checking and giving values to the suits (J,Q,K and A)
    //Contructor, Getters and Settters to the Card Object
    
    public Card(String input) {
        suit = input.charAt(input.length() - 1);
        String temp = input.substring(0, input.length() - 1);
        if(Character.isDigit(temp.charAt(0)))
            value = Integer.parseInt(temp);
        else {
            switch(temp.charAt(0)) {
                case 'J':
                    value = 11;
                    break;
                case 'Q':
                    value = 12;
                    break;
                case 'K':
                    value = 13;
                    break;
                case 'A':
                    value = 14;
                    break;
                default:
                    value = 0;
            }
        }
        
    }

    public int getValue() {
        return value;
    }

    public char getSuit() {
        return suit;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void setSuit(char suit) {
        this.suit = suit;
    }
    
    public String toString() {
        return name;
    }
    
}
