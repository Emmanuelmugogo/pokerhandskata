/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mugoinc.pokerhandskata;

import java.util.Vector;

/**
 *
 * @author Emmanuelmugogo
 */
class Hand {

    private Vector<Card> cards;
    private int combination;
    private int highCard;

    //Constuctor, Getters, Setters and Sorting for a Card Object
    
    public Hand(String[] input) {
        cards = new Vector<Card>(5);
        for (int i = 0; i < input.length; i++) 
            cards.add(new Card(input[i]));
//        sort();
        
    }

    public int getValue(int i) {
        return cards.get(i).getValue();
    }

    public int getSuit(int i) {
        return cards.get(i).getSuit();
    }

    public int getCombination() {
        return combination;
    }

    public int getHighCard() {
        return highCard;
    }
    
    public Card getCard(int i) {
        return cards.get(i);
    }

    public void setCards(Vector<Card> cards) {
        this.cards = cards;
    }

    public void setCombination(int combination) {
        this.combination = combination;
    }

    public void setHighCard(int highCard) {
        this.highCard = highCard;
    }

    private void sort() {
        for(int i = 0; i < cards.capacity(); i++) {
            int indexOfMin = 1, min = getValue(i);
            for (int j = i + 1 ; j < cards.capacity() ; j++) {
                if (getValue(j) < min) {
                    min = getValue(j);
                    indexOfMin = j;
                }
            }
            Card temp = getCard(i);
            cards.set(i, getCard(indexOfMin));
            cards.set(indexOfMin, temp);
        }
    }
    
    public void printHand() {
        for(int i = 0 ; i < cards.capacity() ; i++)
            System.out.println(Integer.toString(getValue(i)) + getSuit(i));
    }
    
}
